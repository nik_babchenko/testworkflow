<?php

// removing unnecessary things

remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );

function disable_rss() {
  return null;
}

add_filter('default_feed', 'disable_rss');
remove_action('wp_head', 'rsd_link');
remove_action('wp_head', 'wlwmanifest_link');
remove_action('wp_head', 'wp_generator');
// add_filter('show_admin_bar', '__return_false');

//registering main menu
function main_menu() {
  register_nav_menu('main-menu',__( 'Главное меню' ));
}
add_action( 'init', 'main_menu' );



//adding scripts
  function news_scripts() {
//jquery
  wp_deregister_script( 'jquery' );

  wp_register_script( 'jquery', 'https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js', false, '1.11.3', true);
  wp_enqueue_script( 'jquery' );

//vendorsripts
  wp_enqueue_script( 'vendorsripts', get_template_directory_uri().'/js/vendorjs.js', 'jquery', null, true);

//yandex sharing
wp_enqueue_script('yandexsharing', 'http://yastatic.net/share2/share.js', 'jquery', null, true);

//mainscript
  wp_enqueue_script('main.js', get_template_directory_uri().'/js/main.js', array('jquery'), null, true);
  }

add_action( 'wp_enqueue_scripts', 'news_scripts' );






//Переводим дату

/**
 * Руссифицирует месяца и недели в дате.
 * Функция для фильтра date_i18n.
 * @param строка $date        Дата в принятом формате
 * @param строка $req_format  Формат передаваемой даты
 * @return Дату в русском формате
 */
function kama_drussify_months( $date, $req_format ){
    // в формате есть "строковые" неделя или месяц
    if( ! preg_match('~[FMlS]~', $req_format ) ) return $date;

    $replace = array (
        "январь" => "января", "Февраль" => "февраля", "Март" => "марта", "Апрель" => "апреля", "Май" => "мая", "Июнь" => "июня", "Июль" => "июля", "Август" => "августа", "Сентябрь" => "сентября", "Октябрь" => "октября", "Ноябрь" => "ноября", "Декабрь" => "декабря",

        "January" => "января", "February" => "февраля", "March" => "марта", "April" => "апреля", "May" => "мая", "June" => "июня", "July" => "июля", "August" => "августа", "September" => "сентября", "October" => "октября", "November" => "ноября", "December" => "декабря",

        "Jan" => "янв.", "Feb" => "фев.", "Mar" => "март.", "Apr" => "апр.", "May" => "мая", "Jun" => "июня", "Jul" => "июля", "Aug" => "авг.", "Sep" => "сен.", "Oct" => "окт.", "Nov" => "нояб.", "Dec" => "дек.",

        "Sunday" => "воскресенье", "Monday" => "понедельник", "Tuesday" => "вторник", "Wednesday" => "среда", "Thursday" => "четверг", "Friday" => "пятница", "Saturday" => "суббота",

        "Sun" => "вос.", "Mon" => "пон.", "Tue" => "вт.", "Wed" => "ср.", "Thu" => "чет.", "Fri" => "пят.", "Sat" => "суб.", "th" => "", "st" => "", "nd" => "", "rd" => "",
    );

    return strtr( $date, $replace );
}
add_filter('date_i18n', 'kama_drussify_months', 11, 2);


  /*
   * Enable support for Post Thumbnails on posts and pages.
   *
   * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
   */
  add_theme_support( 'post-thumbnails' );
  // set_post_thumbnail_size( 250, 270, true );


//цитата

function custom_excerpt_length( $length ) {
    return 20;
}
add_filter( 'excerpt_length', 'custom_excerpt_length', 999 );

function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');

//adding options
add_action( 'admin_init', 'theme_options_init' );
add_action( 'admin_menu', 'theme_options_add_page' );

/**
 * Init plugin options to white list our options
 */
function theme_options_init(){
  register_setting( 'sample_options', 'sample_theme_options', 'theme_options_validate' );
}

/**
 * Load up the menu page
 */
function theme_options_add_page() {
  add_theme_page( __( 'Настройки темы Здравоохранение', 'sampletheme' ), __( 'Настройки темы Здравоохранение', 'sampletheme' ), 'edit_theme_options', 'theme_options', 'theme_options_do_page' );
}


/**
 * Create the options page
 */
function theme_options_do_page() {
  if ( !isset( $_REQUEST['settings-updated'] ) )
    $_REQUEST['settings-updated'] = false;
  ?>

  <div class="wrap">
    <?php screen_icon(); echo "<h2>" . get_current_theme() . __( ': Настройки', 'sampletheme' ) . "</h2>"; ?>

    <?php if ( false !== $_REQUEST['settings-updated'] ) : ?>
    <div class="updated fade"><p><strong><?php _e( 'Options saved', 'sampletheme' ); ?></strong></p></div>
    <?php endif; ?>

    <form method="post" action="options.php">
      <?php settings_fields( 'sample_options' ); ?>
      <?php $options = get_option( 'sample_theme_options' ); ?>

      <table class="form-table">

        <?php
        /**
         * A sample text input option
         */
        ?>
        <tr valign="top"><th colspan="2" style="font-size: 16px;">Координаты в Шапке</th> </tr>
        <tr valign="top"><th scope="row"><?php _e( 'Телефон Приемной', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[phone_one]" class="regular-text" type="text" name="sample_theme_options[phone_one]" value="<?php esc_attr_e( $options['phone_one'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Телефон Отдела обращения граждан', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[phone_two]" class="regular-text" type="text" name="sample_theme_options[phone_two]" value="<?php esc_attr_e( $options['phone_two'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Адрес', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[address]" class="regular-text" type="text" name="sample_theme_options[address]" value="<?php esc_attr_e( $options['address'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'E-mail', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[email]" class="regular-text" type="text" name="sample_theme_options[email]" value="<?php esc_attr_e( $options['email'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th colspan="2" style="font-size: 16px;">Социконки</th> </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Facebook', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[facebook]" class="regular-text" type="text" name="sample_theme_options[facebook]" value="<?php esc_attr_e( $options['facebook'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Youtube', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[youtube]" class="regular-text" type="text" name="sample_theme_options[youtube]" value="<?php esc_attr_e( $options['youtube'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Vkontakte', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[vk]" class="regular-text" type="text" name="sample_theme_options[vk]" value="<?php esc_attr_e( $options['vk'] ); ?>" />
          </td>
        </tr>

        <tr valign="top"><th scope="row"><?php _e( 'Строка возле даты', 'sampletheme' ); ?></th>
          <td>
            <input id="sample_theme_options[event]" class="regular-text" type="text" name="sample_theme_options[event]" value="<?php esc_attr_e( $options['event'] ); ?>" />
          </td>
        </tr>
      </table>

      <p class="submit">
        <input type="submit" class="button-primary" value="<?php _e( 'Save Options', 'sampletheme' ); ?>" />
      </p>
    </form>
  </div>
  <?php
}
function theme_options_validate( $input ) {
  global $select_options, $radio_options;

  // Our checkbox value is either 0 or 1
  if ( ! isset( $input['option1'] ) )
    $input['option1'] = null;
  $input['option1'] = ( $input['option1'] == 1 ? 1 : 0 );

  // Say our text option must be safe text with no HTML tags
  $input['sometext'] = wp_filter_nohtml_kses( $input['sometext'] );

  // Our select option must actually be in our array of select options
  if ( ! array_key_exists( $input['selectinput'], $select_options ) )
    $input['selectinput'] = null;

  // Our radio option must actually be in our array of radio options
  if ( ! isset( $input['radioinput'] ) )
    $input['radioinput'] = null;
  if ( ! array_key_exists( $input['radioinput'], $radio_options ) )
    $input['radioinput'] = null;

  // Say our textarea option must be safe text with the allowed tags for posts
  $input['sometextarea'] = wp_filter_post_kses( $input['sometextarea'] );

  return $input;
}

//end of theme options


//breadcrumbs

function my_breadcrumbs() {

  /* === ОПЦИИ === */
  $text['home'] = 'Главная'; // текст ссылки "Главная"
  $text['category'] = 'Архив рубрики "%s"'; // текст для страницы рубрики
  $text['search'] = 'Результаты поиска по запросу "%s"'; // текст для страницы с результатами поиска
  $text['tag'] = 'Записи с тегом "%s"'; // текст для страницы тега
  $text['author'] = 'Статьи автора %s'; // текст для страницы автора
  $text['404'] = 'Ошибка 404'; // текст для страницы 404
  $text['page'] = 'Страница %s'; // текст 'Страница N'
  $text['cpage'] = 'Страница комментариев %s'; // текст 'Страница комментариев N'

  $wrap_before = '<div class="page-heading__breadcrumbs breadcrumbs"> '; // открывающий тег обертки
  $wrap_after = '</div><!-- .breadcrumbs -->'; // закрывающий тег обертки
  $sep = '›'; // разделитель между "крошками"
  $sep_before = '<div class="breadcrumbs__item breadcrumbs__item--separator">&nbsp;'; // тег перед разделителем
  $sep_after = '</div>'; // тег после разделителя
  $show_home_link = 1; // 1 - показывать ссылку "Главная", 0 - не показывать
  $show_on_home = 0; // 1 - показывать "хлебные крошки" на seой странице, 0 - не показывать
  $show_current = 1; // 1 - показывать название текущей страницы, 0 - не показывать
  $before = '<span class="current">'; // тег перед текущей "крошкой"
  $after = '</span>'; // тег после текущей "крошки"
  /* === КОНЕЦ ОПЦИЙ === */

  global $post;
  $home_link = home_url('/');
  $link_before = '';
  $link_after = '';
  $link_attr = ' ';
  $link_in_before = '<span itemprop="title">';
  $link_in_after = '</span>';
  $link = $link_before . '<a href="%1$s"' . $link_attr . '>' . $link_in_before . '%2$s' . $link_in_after . '</a>' . $link_after;
  $frontpage_id = get_option('page_on_front');
  $parent_id = $post->post_parent;
  $sep = ' ' . $sep_before . $sep . $sep_after . ' ';

  if (is_home() || is_front_page()) {

    if ($show_on_home) echo $wrap_before . '<a href="' . $home_link . '">' . $text['home'] . '</a>' . $wrap_after;

  } else {

    echo $wrap_before;
    if ($show_home_link) echo sprintf($link, $home_link, $text['home']);

    if ( is_category() ) {
      $cat = get_category(get_query_var('cat'), false);
      if ($cat->parent != 0) {
        $cats = get_category_parents($cat->parent, TRUE, $sep);
        $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        if ($show_home_link) echo $sep;
        echo $cats;
      }
      if ( get_query_var('paged') ) {
        $cat = $cat->cat_ID;
        echo $sep . sprintf($link, get_category_link($cat), get_cat_name($cat)) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . sprintf($text['category'], single_cat_title('', false)) . $after;
      }

    } elseif ( is_search() ) {
      if (have_posts()) {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['search'], get_search_query()) . $after;
      } else {
        if ($show_home_link) echo $sep;
        echo $before . sprintf($text['search'], get_search_query()) . $after;
      }

    } elseif ( is_day() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y')) . $sep;
      echo sprintf($link, get_month_link(get_the_time('Y'), get_the_time('m')), get_the_time('F'));
      if ($show_current) echo $sep . $before . get_the_time('d') . $after;

    } elseif ( is_month() ) {
      if ($show_home_link) echo $sep;
      echo sprintf($link, get_year_link(get_the_time('Y')), get_the_time('Y'));
      if ($show_current) echo $sep . $before . get_the_time('F') . $after;

    } elseif ( is_year() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . get_the_time('Y') . $after;

    } elseif ( is_single() && !is_attachment() ) {
      if ($show_home_link) echo $sep;
      if ( get_post_type() != 'post' ) {
        $post_type = get_post_type_object(get_post_type());
        $slug = $post_type->rewrite;
        printf($link, $home_link . '/' . $slug['slug'] . '/', $post_type->labels->singular_name);
        if ($show_current) echo $sep . $before . get_the_title() . $after;
      } else {
        $cat = get_the_category(); $cat = $cat[0];
        $cats = get_category_parents($cat, TRUE, $sep);
        if (!$show_current || get_query_var('cpage')) $cats = preg_replace("#^(.+)$sep$#", "$1", $cats);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
        if ( get_query_var('cpage') ) {
          echo $sep . sprintf($link, get_permalink(), get_the_title()) . $sep . $before . sprintf($text['cpage'], get_query_var('cpage')) . $after;
        } else {
          if ($show_current) echo $before . get_the_title() . $after;
        }
      }

    // custom post type
    } elseif ( !is_single() && !is_page() && get_post_type() != 'post' && !is_404() ) {
      $post_type = get_post_type_object(get_post_type());
      if ( get_query_var('paged') ) {
        echo $sep . sprintf($link, get_post_type_archive_link($post_type->name), $post_type->label) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . $post_type->label . $after;
      }

    } elseif ( is_attachment() ) {
      if ($show_home_link) echo $sep;
      $parent = get_post($parent_id);
      $cat = get_the_category($parent->ID); $cat = $cat[0];
      if ($cat) {
        $cats = get_category_parents($cat, TRUE, $sep);
        $cats = preg_replace('#<a([^>]+)>([^<]+)<\/a>#', $link_before . '<a$1' . $link_attr .'>' . $link_in_before . '$2' . $link_in_after .'</a>' . $link_after, $cats);
        echo $cats;
      }
      printf($link, get_permalink($parent), $parent->post_title);
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && !$parent_id ) {
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_page() && $parent_id ) {
      if ($show_home_link) echo $sep;
      if ($parent_id != $frontpage_id) {
        $breadcrumbs = array();
        while ($parent_id) {
          $page = get_page($parent_id);
          if ($parent_id != $frontpage_id) {
            $breadcrumbs[] = sprintf($link, get_permalink($page->ID), get_the_title($page->ID));
          }
          $parent_id = $page->post_parent;
        }
        $breadcrumbs = array_reverse($breadcrumbs);
        for ($i = 0; $i < count($breadcrumbs); $i++) {
          echo $breadcrumbs[$i];
          if ($i != count($breadcrumbs)-1) echo $sep;
        }
      }
      if ($show_current) echo $sep . $before . get_the_title() . $after;

    } elseif ( is_tag() ) {
      if ( get_query_var('paged') ) {
        $tag_id = get_queried_object_id();
        $tag = get_tag($tag_id);
        echo $sep . sprintf($link, get_tag_link($tag_id), $tag->name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_current) echo $sep . $before . sprintf($text['tag'], single_tag_title('', false)) . $after;
      }

    } elseif ( is_author() ) {
      global $author;
      $author = get_userdata($author);
      if ( get_query_var('paged') ) {
        if ($show_home_link) echo $sep;
        echo sprintf($link, get_author_posts_url($author->ID), $author->display_name) . $sep . $before . sprintf($text['page'], get_query_var('paged')) . $after;
      } else {
        if ($show_home_link && $show_current) echo $sep;
        if ($show_current) echo $before . sprintf($text['author'], $author->display_name) . $after;
      }

    } elseif ( is_404() ) {
      if ($show_home_link && $show_current) echo $sep;
      if ($show_current) echo $before . $text['404'] . $after;

    } elseif ( has_post_format() && !is_singular() ) {
      if ($show_home_link) echo $sep;
      echo get_post_format_string( get_post_format() );
    }

    echo $wrap_after;

  }
} // end of my_breadcrumbs()
// end of breadcrumbs





 ?>
