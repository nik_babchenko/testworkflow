<?php get_header(); ?>
     <!--  slider start -->
     <?php if (is_front_page()): ?>
      <section class="slider">
        <div class="owl-carousel">
        <?php
          $args = array(


            //Type & Status Parameters
            'post_status' => 'publish',

            //Order & Orderby Parameters
            'order'               => 'DESC',
            'orderby'             => 'date',

            //Pagination Parameters
            'posts_per_page'         => 3,
          );

        $the_query = new WP_Query( $args );
         ?>

  <?php if( $the_query->have_posts() ): ?>
  <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>

          <div class="owl-carousel__item item">
            <div class="container slider-wrapper">
              <div class="slider-content">
                <div class="slider-content__image">
                  <?php the_post_thumbnail(array('400, 250'), true); ?>
                </div>
                <div class="slider-description-wrapper">
                  <div class="slider-description">
                    <div class="slider-description__item slider-description__item--heading">
                      <?php the_title(); ?>
                    </div>
                    <div class="slider-description__item slider-description__item--text">
                      <?php the_excerpt(); ?>
                    </div>
                    <div class="slider-description__item slider-description__item--read-more">
                      <a href="<?php the_permalink(); ?>">Подробнее<i class="fa fa-angle-right"></i></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

  <?php endwhile; ?>
  <?php endif; ?>
  <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
        </div>
      </section>

    <!-- slider end -->

      <div class="section nav-icons">
        <div class="container">
          <div class="med-icons">
            <a class="med-icons__item" href="<?php echo get_category_link('3'); ?>"><i class="flaticon-gear39"></i>
              <div class="programm-heading">
              <?php echo get_the_category_by_ID('3'); ?>
              </div>
            </a><a class="med-icons__item" href="<?php echo get_category_link('4'); ?>"><i class="flaticon-tv30"></i>
              <div class="programm-heading">
                Мы<br />в СМИ
              </div>
            </a><a class="med-icons__item" href="<?php echo get_category_link('5'); ?>"><i class="flaticon-heart258"></i>
              <div class="programm-heading">
                Доска<br />почета
              </div>
            </a><a class="med-icons__item" href="<?php echo get_category_link('6'); ?>"><i class="flaticon-caduceus4"></i>
              <div class="programm-heading">
                История медицины
              </div>
            </a><a class="med-icons__item" href="<?php echo get_category_link('7'); ?>"><i class="flaticon-hospital11"></i>
              <div class="programm-heading">
                Городские программы
              </div>
            </a>
          </div>
        </div>
      </div>
  <?php endif ?>


      <div class="content">
        <div class="container">

        <?php get_template_part('sidebar-left'); ?>

          <div class="content-main col-lg-6 col-md-6 col-xs-6">
            <div class="content-container__wrapper equal">
              <div class="content-container">
                <div class="content-container__category">
                  <div class="news__heading">
                    Новости
                  </div>

<?php
  /**
   * The WordPress Query class.
   * @link http://codex.wordpress.org/Function_Reference/WP_Query
   *
   */
  $args = array(

    //Post & Page Parameters
    // 'post__not_in' => array(1,2,3),


    //Category Parameters
    // 'category__not_in' => array( 1, 2 ),

    //Type & Status Parameters
    'post_status' => 'publish',
    //Choose ^ 'any' or from below, since 'any' cannot be in an array
    'post_type' => array(
      'post',
      ),

    //Order & Orderby Parameters
    'order'               => 'DESC',
    'orderby'             => 'date',
    'paged'                  => get_query_var('paged'),

  );



$the_query = new WP_Query( $args );


 ?>

           <?php if( $the_query->have_posts() ): ?>
           <?php while( $the_query->have_posts() ) : $the_query->the_post(); ?>
                  <a class="news-item" href="<?php the_permalink();?>">
                    <div class="news-item__image">
                      <?php the_post_thumbnail(array(160,160), true); ?>
                    </div>
                    <div class="news-item__content news-content">
                      <h3 class="news-content__heading">
                        <?php the_title(); ?>
                      </h3>
                      <div class="news-content__date">
                        <?php echo get_the_date(); ?>
                      </div>
                      <div class="news-content__text">
                        <?php echo get_the_excerpt(); ?>
                      </div>
                    </div>
                  </a>
          <?php endwhile; ?>

          <?php the_posts_pagination(array(
           'screen_reader_text' => ' ',
          )); ?>

          <?php endif; ?>
          <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-3 col-md-3 col-xs-3 content-sidebar equal">
            <div class="sidebar-container">
              <div class="sidebar-container__category">
                Горячая тема
              </div>
              <div class="sidebar-container__image">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/news-hot.jpg" />
              </div>
              <div class="sidebar-container__heading">
                Почему сопротивление?
              </div>
              <div class="sidebar-container__content">
                Преамбула антиконституционна. В соответствии с общим принципом, установленным Конституцией РФ, акцепт заключен.
              </div>
              <a class="sidebar-container__btn" href="#">Подробнее</a>
            </div>
          </div>
        </div>
      </div>

<?php get_footer(); ?>
