
          <div class="content-sidebar col-lg-3 col-md-3 col-xs-3 equal">
            <div class="sidebar-container">
              <div class="sidebar-container__category">
                Одесский врач
              </div>
              <div class="sidebar-container__image">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/doctor.jpg" />
              </div>
              <div class="sidebar-container__content">
                Преамбула антиконституционна. В соответствии с общим принципом, установленным Конституцией РФ, акцепт заключен.
              </div>
              <a class="sidebar-container__btn" href="#">Подробнее</a>
            </div>


            <div class="sidebar-container">
              <div class="sidebar-container__category">
                Важно
              </div>
              <?php
                /**
                 * The WordPress Query class.
                 * @link http://codex.wordpress.org/Function_Reference/WP_Query
                 *
                 */
                $args_new = array(
                  //Type & Status Parameters
                  'post_type'=>'vajno'
                );
              $vajno = new WP_Query( $args_new );

               ?>

          <?php if( $vajno->have_posts() ): ?>
          <?php while( $vajno->have_posts() ) : $vajno->the_post(); ?>
        <?php if (get_field('link')) {
          $a=get_field('link');
        }
         else {$a=get_category_link(get_field('category'));  } ?>
        <a class="sidebar-container__image" href="<?php echo $a; ?>">
       <img  src="<?php the_field('image') ?>" > </a>

  <?php endwhile; ?>
  <?php endif; ?>
  <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>

            </div>
          </div>
