<?php
  get_header();
?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

      <div class="page-content-wrapper">
        <div class="container">
          <div class="content-sidebar col-lg-3 col-md-3 col-xs-3 equal">
            <div class="sidebar-container">
              <div class="sidebar-container__category">
                Одесский врач
              </div>
              <div class="sidebar-container__image">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/doctor.jpg" />
              </div>
              <div class="sidebar-container__content">
                Преамбула антиконституционна. В соответствии с общим принципом, установленным Конституцией РФ, акцепт заключен.
              </div>
              <a class="sidebar-container__btn" href="#">Подробнее</a>
            </div>
            <div class="sidebar-container">
              <div class="sidebar-container__category">
                Важно
              </div>
              <a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/zdorov.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/vich.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/trauma.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/beshen.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/equipment.jpg" /></a>
            </div>
          </div>
          <div class="content-main col-xs-9 equal">
          <div class="page-content content-container__wrapper equal">
            <h1 class="page-content__header">
              <?php the_title(); ?>
            </h1>
            <div class="page-breadcrumbs">
             <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
            </div>
 <!--            <div class="page-content__meta page-meta">
              <div class="page-date">
                <strong>Опубликовано:</strong><span>  <?php echo get_the_date(); ?></span>
              </div>
            </div> -->

            <div class="page-content__image">
              <?php the_post_thumbnail('large', true) ?>
            </div>
            <div class="page-content__text">
            <?php the_content(); ?>
            </div>
    <?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>
          </div>
        </div>
      </div>
      </div>
</div>

<?php
  get_footer();
?>
