<?php get_header(); ?>
      <div class="content">
        <div class="container">
          <div class="content-sidebar col-lg-3 col-md-3 col-xs-3 equal">
            <div class="sidebar-container">
              <div class="sidebar-container__category">
                Одесский врач
              </div>
              <div class="sidebar-container__image">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/doctor.jpg" />
              </div>
              <div class="sidebar-container__content">
                Преамбула антиконституционна. В соответствии с общим принципом, установленным Конституцией РФ, акцепт заключен.
              </div>
              <a class="sidebar-container__btn" href="#">Подробнее</a>
            </div>
            <div class="sidebar-container">
              <div class="sidebar-container__category">
                Важно
              </div>
              <a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/zdorov.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/vich.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/trauma.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/beshen.jpg" /></a><a class="sidebar-container__image" href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/equipment.jpg" /></a>
            </div>
          </div>
          <div class="content-main col-lg-9 col-md-9 col-xs-9">
            <div class="content-container__wrapper equal">
              <div class="content-container">
                <div class="content-container__category">
                  <div class="news__heading">
                    <?php printf( __( 'Результаты поиска по: %s', 'shape' ), '<span>"' . get_search_query() . '</span>"' ); ?>
                  </div>

           <?php if( have_posts() ): ?>
           <?php while( have_posts() ) : the_post(); ?>
                  <a class="news-item" href="<?php the_permalink();?>">
                    <div class="news-item__image">
                      <?php the_post_thumbnail(array(160,160), true); ?>
                    </div>
                    <div class="news-item__content news-content">
                      <h3 class="news-content__heading">
                        <?php the_title(); ?>
                      </h3>
                      <div class="news-content__date">
                        <?php echo get_the_date(); ?>
                      </div>
                      <div class="news-content__text">
                        <?php echo get_the_excerpt(); ?>
                      </div>
                    </div>
                  </a>
          <?php endwhile; ?>
          <?php else: ?>
          <h3>По данному запросу ничего не найдено</h3>
          <?php endif; ?>
          <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

<?php get_footer(); ?>
