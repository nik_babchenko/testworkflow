<!DOCTYPE html>
<html lang="ru">
  <head>
    <meta charset="utf-8" />
    <meta content="IE=edge" http-equiv="X-UA-Compatible" />
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <title>Департамент здравоохранения Одесского городского совета</title>
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/stylesheets/vendorstyles.css" rel="stylesheet" />
    <link href="<?php echo get_stylesheet_directory_uri(); ?>/stylesheets/style.css" rel="stylesheet" />
    <?php wp_head(); ?>
  </head>
  <body>
    <div class="wrapper-main">
      <div class="header">
        <div class="header-wrapper">
          <div class="header__main">
            <div class="header-main-cover"></div>
            <div class="container">
              <div class="row">
                <div class="header-main-in">
                  <div class="header-logo">
                    <div class="header-logo__item">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/logo.png" />
                    </div>
                  </div>
                  <div class="header-info">
                    <div class="header-info__item header-info__item-text-logo">
                      <h1 class="text-logo">
                        Департамент здравоохранения Одесского городского совета
                      </h1>
                    </div>
                    <div class="header-info__item header-info__item-coordinates">
                      <div class="header-contacts__item">
                        <i class="flaticon-cellphone57"></i>
                        <div class="contacts-label">
                          Приемная:<div><?php $options = get_option('sample_theme_options');
                echo $options['phone_one']; ?></div>
                        </div>
                      </div>
                      <div class="header-contacts__item">
                        <i class="flaticon-cellphone57"></i>
                        <div class="contacts-label">
                          Отдел обращений граждан: <div><?php echo $options['phone_two'] ?></div>
                        </div>
                      </div>
                      <div class="header-contacts__item">
                        <i class="flaticon-location74"></i>
                        <div class="contacts-label">
                          Адрес:
                          <div class="contacts-address">
                            <?php echo $options['address'] ?>
                          </div>
                        </div>
                      </div>
                      <div class="header-contacts__item">
                        <i class="flaticon-letter121"></i>
                        <div class="contacts-label">
                          E-mail:<a href="mailto:<?php echo $options['email'] ?>"><?php echo $options['email'] ?></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="header-connections">
                    <div class="header-connections__item searchform">
                    <?php get_template_part('searchform'); ?>
                    </div>
                    <div class="header-connections__item header-connections__item--application">
                      <a class="application-item" data-featherlight="#appeal" href="#">Оставить обращение</a>
                      <div class="appeal" id="appeal">

                        <form id="appeal-form">
                          <div class="appeal-header">
                            Оставить обращение
                          </div>
                          <input class="appeal-input" placeholder="Ваше имя" require="" type="text" /><input class="appeal-input" placeholder="Ваш e-mail" require="" type="email" /><input class="appeal-input" placeholder="Тема обращения" type="text" /><textarea class="appeal-textarea" placeholder="Здравствуйте! Меня зовут Иванов Иван Иванович и я хочу задать вопрос по поводу..."></textarea><input id="submit-appeal" type="submit" />
                        </form>


                      </div>
                    </div>
                    <div class="header-connections__item social-icons">
                      <a class="social-icons__item social-icons__item--facebook" href="<?php echo $options['facebook']; ?>" target="_blank"></a><a class="social-icons__item social-icons__item--youtube" href="<?php echo $options['youtube']; ?>" target="_blank"></a><a class="social-icons__item social-icons__item--vk" href="<?php echo $options['vk']; ?>" target="_blank"></a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="header__date additional">
            <div class="container">
              <div class="row">
                <div class="additional-date col-lg-3 col-md-3">
                 <time> <?php echo date_i18n('d F Y'); ?> года, <?php echo date_i18n('H:i'); ?> </time>
                </div>
                <div class="additional-string col-lg-9 col-md-9">
                  <?php echo $options['event'] ?>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="header__menu">
          <div class="container">
            <div class="row">
          <?php wp_nav_menu( array(
            'theme_location'  => 'main-menu',
            'menu'            => 'main-menu',
            'container'       => '',
            'container_class' => '',
            'container_id'    => '',
            'menu_class'      => 'menu',
            'menu_id'         => 'menu',
            'echo'            => true,
            'fallback_cb'     => 'wp_page_menu',
            'before'          => '',
            'after'           => '',
            'link_before'     => '',
            'link_after'      => '',
            'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
            'depth'           => 0
          )); ?>
            </div>
          </div>
        </div>
      </div>
