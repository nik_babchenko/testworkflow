<?php get_header(); ?>
      <div class="content">
        <div class="container">
          <?php get_template_part('sidebar-left') ?>
          <div class="content-main col-lg-9 col-md-9 col-xs-9">
            <div class="content-container__wrapper equal">
              <div class="content-container">
                <div class="content-container__category">
                  <div class="news__heading">
                    КАТЕГОРИЯ: <?php single_tag_title();  ?>
                  </div>

           <?php if( have_posts() ): ?>
           <?php while( have_posts() ) : the_post(); ?>
                  <a class="news-item" href="<?php the_permalink();?>">
                    <div class="news-item__image">
                      <?php the_post_thumbnail(array(160,160), true); ?>
                    </div>
                    <div class="news-item__content news-content">
                      <h3 class="news-content__heading">
                        <?php the_title(); ?>
                      </h3>
                      <div class="news-content__date">
                        <?php echo get_the_date(); ?>
                      </div>
                      <div class="news-content__text">
                        <?php echo get_the_excerpt(); ?>
                      </div>
                    </div>
                  </a>
          <?php endwhile; ?>
           <?php the_posts_pagination(array(
           'screen_reader_text' => ' ',
          )); ?>
          <?php else: ?>
          <p>Записи в данной категории отсутствуют</p>
          <?php endif; ?>
          <?php wp_reset_query();  // Restore global post data stomped by the_post(). ?>
                </div>
              </div>
            </div>
          </div>

        </div>
      </div>

<?php get_footer(); ?>
