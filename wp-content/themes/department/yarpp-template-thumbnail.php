<?php
/*
YARPP Template: Thumbnails
Description: Requires a theme which supports post thumbnails
Author: mitcho (Michael Yoshitaka Erlewine)
*/ ?>
<div class="featured-news__header">
  Похожие новости
</div>
<?php if (have_posts()):?>
  <div class="featured-news-in">
	<?php while (have_posts()) : the_post(); ?>
		<?php if (has_post_thumbnail()):?>
      <a class="featured-news-in__item" href="<?php the_permalink();?>">
        <div class="featured-news-in__image">
          <?php the_post_thumbnail(array(320,340), true) ?>
        </div>
        <div class="featured-news-in__meta">
          <div class="meta-date">
            <?php echo get_the_date(); ?>
          </div>
          <div class="meta-content">
            <?php echo get_the_title(); ?>
          </div>
        </div>
      </a>
		<?php endif; ?>
	<?php endwhile; ?>
    </div>
<?php else: ?>
<p>Похожие новости отсутствуют</p>
<?php endif; ?>

