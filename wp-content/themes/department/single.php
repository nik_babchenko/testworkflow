<?php
  get_header();
?>
    <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
      <div class="page-content-wrapper">
        <div class="container">
          <div class="page-content">
            <h1 class="page-content__header">
              <?php the_title(); ?>
            </h1>

            <div class="page-breadcrumbs">
             <?php if (function_exists('my_breadcrumbs')) my_breadcrumbs(); ?>
            </div>
            <div class="page-content__meta page-meta">
              <div class="page-date">
                <strong>Опубликовано:</strong><span>  <?php echo get_the_date(); ?></span>
              </div>
              <div class="page-category">
                <strong>Категория:</strong> &nbsp;<?php the_category(', ', 'single'); ?>
              </div>
            </div>
            <div class="page-content__image">
              <?php the_post_thumbnail('large', true) ?>
            </div>
            <div class="page-content__text">
            <?php the_content(); ?>
            <div class="ya-share2" data-services="vkontakte,facebook,odnoklassniki,gplus" data-counter=""></div>
            </div>

    <?php endwhile; else: ?>
    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
    <?php endif; ?>

            <div class="featured-news">
            <?php related_posts(); ?>
            </div>
            <div class="comments">
            <?php

             // comments_template('/comments.php');

              ?>

            </div>
          </div>
        </div>
      </div>
</div>

<?php
  get_footer();
?>
